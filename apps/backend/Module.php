<?php

namespace CMS\Backend;

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Assets\Manager as AssetsManager;

class Module
{

    public function registerAutoloaders()
    {

        $loader = new Loader();

        $loader->registerNamespaces(array(
            'CMS\Backend\Controllers'   => '../apps/backend/controllers/',
            'CMS\Models'                => '../apps/models/',
            'CMS\Backend\Plugins'       => '../apps/backend/plugins/',
            'CMS\Backend\Forms'         => '../apps/backend/forms/',
            'Library'                   => '../library/',
        ));

        $loader->register();
    }

    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices($di)
    {

        $config = require __DIR__ . "/config/config.php";

        //Registering a dispatcher
        $di->set('dispatcher', function() {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("CMS\\Backend\\Controllers\\");
            return $dispatcher;
        });

        //Registering the view component
        $di->set('view', function() {
            $view = new View();
            $view->setViewsDir('../apps/backend/views/');
            return $view;
        });

        $di->set('config', function() use($config) {
            return $config;
        });

        $di->set('assets', function () {
            $assets = new AssetsManager();

//          $assets->addCss('css/admin/style.css');
//          $assets->addCss('css/main/basic.css', true);

            $assets->collection('style')
                ->addCss('components/bootstrap/dist/css/bootstrap.min.css', true)
                ->addCss('components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css', true)
                ->addCss('components/components-font-awesome/css/font-awesome.min.css', true);
//              ->setSourcePath('css/main/admin.css')
//              ->setTargetUri('css/main/admin.css')
//              ->join(true)
//              ->addFilter(new \Phalcon\Assets\Filters\Cssmin());
            
            $assets->collection('jsHead')
                ->addJs('components/jquery/dist/jquery.min.js', true);
            
            $assets->collection('jsTools');
            
            return $assets;
        });

    }
}
