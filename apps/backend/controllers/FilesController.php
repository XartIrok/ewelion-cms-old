<?php 

namespace CMS\Backend\Controllers;

use Library\myTools;
use CMS\Models\UploadFolder;
use CMS\Models\Upload;
use Phalcon\Image\Adapter\Gd;
use Phalcon\Paginator\Adapter\NativeArray as Paginator;

class FilesController extends ControllerBase
{
    private $lista = array();
    private $dirFiles;
    private $dirUpload;
    private $folderId;
    private $lists;

    public function initialize()
    {
        parent::initialize();
        $this->assets->get('jsHead')->addJs('components/jquery-uploadfile/js/jquery.uploadfile.min.js', true);

        $this->dirFiles  = APP_PATH . 'files';
        $this->dirUpload = APP_PATH . 'public/uploads';
        
        if($this->router->getActionName() == 'addUpload' or $this->router->getActionName() == 'addLink')
        {
            // public folder must to be change permision to 777
            @mkdir($this->dirFiles);
            @mkdir($this->dirUpload);
        }
    }

    public function indexAction($folderId = 1, $page = 1)
    {
        $this->getMainUpload($folderId, $page);

        $place = UploadFolder::findFirst(array("id = 1"));

        $this->lista[] = array('level' => 1, 'folder' => $place);
        $this->getChildFolder($place->id, 1);

        $this->view->objects = $this->lists->getPaginate();
        $this->view->lists = $this->lista;
        $this->view->folderId = $folderId;
    }

    public function browseAction($folderId = 1, $page = 1)
    {
        $this->getMainUpload($folderId, $page, true);

        $this->view->objects = $this->lists->getPaginate();
        $this->view->folderId = $folderId;
        $this->view->setMainView('browse');
    }

    public function galleryAction($folderId = 1, $page = 1)
    {
        $this->getMainUpload($folderId, $page, true, true);

        $this->view->objects = $this->lists->getPaginate();
        $this->view->folderId = $folderId;
        $this->view->setMainView('browse');
        $this->view->pick("files/browse");
    }

    private function getMainUpload($folderId, $page, $browser = false, $gallery = false)
    {
        $folderMain = UploadFolder::findFirst(array(
            "id = :id:",
            'bind' => array('id' => $folderId)
        ));

        $folders = UploadFolder::find(array(
            "parent = :id:",
            'bind' => array('id' => $folderMain->id),
            'order' => 'html_name'
        ));

        $q = "folder_id = :id:";
        if($browser)
            $q .= " AND enabled = 1";
        if($gallery)
            $q .= " AND ext IN('jpg', 'jpeg', 'png', 'gif')";

        $uploads = Upload::find(array(
            $q,
            'bind' => array('id' => $folderMain->id),
            'order' => 'name'
        ));

        foreach ($folders as $folder) {
            $arr[] = array(
                'id' => $folder->id,
                'name' => $folder->html_name,
                'type' => 'folder',
                'enabled' => $folder->enabled,
                'name_ext' => null,
                'link' => null,
                'ext' => null,
                'date_create' => null
            );
        }

        foreach ($uploads as $upload) {
            $arr[] = array(
                'id' => $upload->id,
                'name' => $upload->name,
                'type' => 'file',
                'enabled' => $upload->enabled,
                'name_ext' => $upload->name_ext,
                'link' => $upload->link,
                'ext' => $upload->ext,
                'date_create' => $upload->date_create
            );
        }

        $this->lists = new Paginator(
            array(
                "data"  => $arr,
                "limit" => 10,
                "page"  => $page
            )
        );
    }

    public function addUploadAction($folderId)
    {
        $this->view->disable();

        if(isset($_FILES["upload_cms"]))
        {
            $ret = array();
            
            $output_dir = $this->dirFiles.'/'.date('Y/m/d', time());
            @mkdir($output_dir, 0744, true);

            //  This is for custom errors;  
            /*  $custom_error= array();
                $custom_error['jquery-upload-file-error']="File already exists";
                echo json_encode($custom_error);
                die();
            */
            $error = $_FILES["upload_cms"]["error"];
            //You need to handle  both cases
            //If Any browser does not support serializing of multiple files using FormData() 
            if(!is_array($_FILES["upload_cms"]["name"])) //single file
            {
                $fileName = strtolower($_FILES["upload_cms"]["name"]);

                if($this->saveUpload($fileName, $folderId))
                {
                    $ret[] = $fileName;
                    move_uploaded_file($_FILES["upload_cms"]["tmp_name"], $output_dir.DIRECTORY_SEPARATOR.$fileName);
                    $this->generateThumbnail($output_dir.DIRECTORY_SEPARATOR.$fileName, $fileName);
                }
            }
            echo json_encode($ret);
        }
    }

    public function addLinkAction($folderId)
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('files/index/'.$folderId);
        }

        $data = $this->request->getPost();
        
        @mkdir($this->dirUpload.'/link');

        $fullName = strtolower(basename($data['urlAdres']));
        $ext = substr(strrchr($fullName, '.'), 1 );

        if(in_array($ext, array('pdf', 'doc', 'docx', 'jpg', 'jpeg', 'png', 'gif')))
        {
            if($this->saveUpload($fullName, $folderId, 1))
            {
                file_put_contents($this->dirUpload.'/link/'.$fullName, file_get_contents($data['urlAdres']));

                $this->flashSession->success('Plik został dodany');
                return $this->response->redirect('files/index/'.$folderId);
            }
            else
            {
                $this->flashSession->error('Nie udało się zapisać pliku.');
                return $this->response->redirect('files/index/'.$folderId);
            }
        }
        else
        {
            $this->flashSession->error('Plik który chcesz dodać ma nie poprawny format.');
            return $this->response->redirect('files/index/'.$folderId);
        }
    }

    private function saveUpload($fullName, $folderId, $link = 0)
    {
        $_ext = strrchr($fullName, '.');
        $name = substr($fullName, 0, -(strlen($_ext)));
        $ext  = substr($_ext, 1);

        $upload = new Upload();
        $upload->folder_id = $folderId;
        $upload->hash = myTools::genMd5(32);
        $upload->name = myTools::nameFile($name);
        $upload->ext = $ext;
        $upload->name_ext = $fullName;
        $upload->link = $link;

        if($upload->save())
            return true;
        else
            return false;
    }

    public function deleteFileAction($folderId, $id)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('files/index/'.$folderId);
        }

        $upload = Upload::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if ($upload) {
            if($upload->link == 0)
            {
                $date = date_create($upload->date_create);
                unlink($this->dirFiles.DIRECTORY_SEPARATOR.$date->format('Y/m/d').DIRECTORY_SEPARATOR.$upload->name_ext);
            }
            else
                unlink($this->dirUpload.DIRECTORY_SEPARATOR.'link'.DIRECTORY_SEPARATOR.$upload->name_ext);
                
            $upload->delete();
            echo "SUCCESS||Plik został poprawnie usunięty";
        }
        else
            echo "DANGER||Plik który chcesz usunąć nieistnieje";
    }

    public function addFolderAction($folderId)
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('files/index/'.$folderId);
        }

        $data = $this->request->getPost();

        if(strlen($data['folderName']) < 2) 
        {
            $this->flashSession->error('Nazwa folderu nie może być krótsza niż 2 znaki');
            return $this->response->redirect('files/index/'.$folderId);
        }

        $folder = new UploadFolder();
        $folder->parent = $folderId;
        $folder->html_name = $data['folderName'];
        $folder->enabled = 1;
        $folder->undelete = 0;
        $folder->save();

        $this->flashSession->success('Folder został utworzony');
        return $this->response->redirect('files/index/'.$folderId);
    }

    public function deleteFolderAction($folderId, $id)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('files/index/'.$folderId);
        }

        $folder = UploadFolder::findFirst(array(
            "id = :id:",
            'bind' => array('id' => $id)
        ));

        $uploads = Upload::find(array(
            "folder_id = :id:",
            'bind' => array('id' => $folder->id)
        ));

        $folders = UploadFolder::find(array(
            "parent = :id:",
            'bind' => array('id' => $folder->id)
        ));

        if(count($folders) > 0 || count($uploads) > 0)
        {
            echo "DANGER||Folder który chcesz usunąć nie jest pusty, wpierw go opróżnij";
            return;
            // $this->flashSession->error('Nie można usunąć folderu bo nie jest pusty');
            // return $this->response->redirect('files/index/'.$folderId);
        }

        $folder->delete();
        echo "SUCCESS||Folder został porapwnie usuniety";
        // $this->flashSession->success('Folder został porapwnie usuniety');
        // return $this->response->redirect('files/index/'.$folder->parent);
    }

    public function visibleFolderAction($folderId, $value)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('files/index/'.$folderId);
        }
        
        $folder = UploadFolder::findFirst(array('id = :id:', 'bind' => array('id' => $folderId)));

        $folder->enabled = $value;

        if($folder->save())
        {
            echo "SUCCESS||Zmieniono status folderu";
        }
        else
            echo "DANGER||Dany folder nie istnieje nie można było zastosować zmian";
    }

    private function getChildFolder($parentId, $level)
    {
        $folders = UploadFolder::find(array(
            "parent = :id:",
            'bind' => array('id' => $parentId),
            'order' => 'html_name'
        ));

        $level += 1;

        foreach ($folders as $folder) {
            $this->lista[] = array('level' => $level, 'folder' => $folder);
            $this->getChildFolder($folder->id, $level);
        }
    }
    
    public function saveCheckAction()
    {
        $this->view->disable();
        if($this->request->isPost())
        {
            $images = $this->request->getPost('checkId');
            $this->session->set('imageCheck', $images);
        }
    }
    
    private function generateThumbnail($path, $name)
    {
        $ext = substr(strrchr($name, '.'), 1);
        
        if(in_array($ext, array('jpg', 'jpeg', 'png', 'gif')))
        {
            $dir = $this->dirUpload.DIRECTORY_SEPARATOR.'thumbnail'.DIRECTORY_SEPARATOR.date('Y/m/d');
            @mkdir($dir, 0744, true);
            
            $img = new Gd($path);
            $img->resize(300, 300);
            $img->save($dir.DIRECTORY_SEPARATOR.$name, 90);
        }
    }
}