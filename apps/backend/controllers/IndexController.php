<?php

namespace CMS\Backend\Controllers;

use CMS\Models\UserAccount;
use CMS\Models\UserInformation;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->assets->get('style')->addCss('css/admin/login.css');
        $this->assets->get('jsTools')->addJs('js/admin.js', true);
        $this->view->setMainView('login');

        if($this->session->get($this->config->session->name))
            $this->response->redirect('admin/main');

        return parent::initialize();
    }

    public function indexAction() 
    {
        
    }

    private function auth(UserAccount $user)
    {
        $this->session->set($this->config->session->name, array(
            'id' => $user->id,
            'name' => $user->login
        ));
    }

    public function loginAction()
    {
        if ($this->request->isPost()) {
            $user = UserAccount::findFirst(array(
                "login = :login: AND is_active = 1",
                'bind' => array('login' => $this->request->getPost('logi'))
            ));

            if ($user && $this->security->checkHash($this->request->getPost('pass'), $user->pass)) {
                $userInformation = UserInformation::findFirst(array('user_id = :id:', 'bind' => array('id' => $user->id)));
                $userInformation->date_lastactive = time();
                $userInformation->save();
                $this->auth($user);

                $this->flashSession->success('Witaj: '.$user->login);
                return $this->response->redirect('main');
            }
            else {
                $this->flashSession->error('Wprowadzone dane sa nie poprawne');
                return $this->response->redirect('index');
            }
        }
        else
            return $this->response->redirect('index');
    }

}
