<?php 

namespace CMS\Backend\Controllers;

use Library\myTools;
use Phalcon\Mvc\View;

use CMS\Models\Blog;
use CMS\Models\BlogCategory;
use CMS\Models\BlogComment;

use CMS\Backend\Forms\BlogForm;
use CMS\Backend\Forms\BlogCategoryForm;

class BlogController extends ControllerBase
{
    private $list = array();
    private $meni = array();

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction($category, $page)
    {
        $articles = Blog::find(array(
            'order' => 'date_create DESC',
        ));

        $this->view->articles = $articles;
    }

    public function newAction()
    {
        $this->view->form = new BlogForm(null, array('edit' => false));
    }

    public function addAction()
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('blog/index');
        }

        $blog = new Blog();
        $form = new BlogForm();

        $data = $this->request->getPost();
        $form->bind($_POST, $blog);

        if(!$form->isValid())
        {
            $this->flashSession->error('Formularz jest nie poprawnie wypełniony');
            return false;
        }

        if($blog->save() == false)
        {
            $this->flashSession->error('Nie udało się zapisać artykułu');
            return false;
        }
        else
        {
            $this->flashSession->success('Artykuł została poprawnie zapisana');
            return $this->response->redirect('blog/edit/'.$blog->id);
        }

        $this->view->pick('blog/new');
    }

    public function editAction($id)
    {
        $blog = Blog::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$blog)
        {
            $this->flashSession->error('Artykuł o podanym id nie istnieje');
            return $this->response->redirect('blog/index');
        }

        $this->view->linkId = $blog->id;
        $this->view->linkTitle = myTools::clearLink($blog->html_name);

        $this->view->form = new BlogForm($blog, array('edit' => true));
    }

    public function updateAction()
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('blog/index');
        }
        $id = $this->request->getPost('id');

        $blog = Blog::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$blog)
        {
            $this->flashSession->error('Artykuł o podanym id nie istnieje');
            return $this->response->redirect('blog/index');
        }

        $form = new BlogForm();
        $this->view->form = $form;

        $data = $this->request->getPost();
        $form->bind($data, $blog);

        if(!$form->isValid())
        {
            $this->flashSession->error('Formularz jest nie poprawnie wypełniony');
            return false;
        }

        if($blog->save() == false)
        {
            $this->flashSession->error('Nie udało zaktualizować artykułu');
            return false;
        }
        else
        {
            $this->flashSession->success('Artykuł została pomyślnie zaktualizowana');
            return $this->response->redirect('blog/edit/'.$blog->id);
        }

        $this->view->pick('blog/edit');
    }

    public function deleteArtAction($id)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('pages');
        }

        $category = Blog::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if($category)
        {
            $category->delete();
            echo "SUCCESS||Strona została poprawnie usunięta";
        }
        else
            echo "DANGER||Dana strona nie istnieje nie można jej usunąć";
    }

    public function enabledArtAction($id, $value)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('pages');
        }
        
        $category = Blog::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        $category->enabled = $value;

        if($category->save())
        {
            echo "SUCCESS||Zmieniono status strony";
        }
        else
            echo "DANGER||Dana strona nie istnieje nie można było zastosować zmian";
    }

    public function listCategoryAction()
    {
        $cats = BlogCategory::find(array(
            "parent = 0",
        ));

        $level = 1;

        foreach ($cats as $cat) {
            $this->list[] = array('level' => $level, 'cat' => $cat);
            $this->childCategory($cat->id, $level);
        }

        $this->view->categories = $this->list; 
    }

    private function childCategory($parent, $level)
    {
        $cats = BlogCategory::find(array(
            "parent = :id:",
            'bind' => array('id' => $parent)
        ));

        $level += 1;

        foreach ($cats as $cat) {
            $this->list[] = array('level' => $level, 'cat' => $cat);
            $this->childCategory($cat->id, $level);
        }
    }

    public function newCategoryAction()
    {
        $this->view->form = new BlogCategoryForm(null, array('edit' => false));
    }

    public function addCategoryAction()
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('blog/listCategory');
        }

        $blog = new BlogCategory();
        $form = new BlogCategoryForm();

        $data = $this->request->getPost();
        $form->bind($_POST, $blog);

        if(!$form->isValid())
        {
            $this->flashSession->error('Formularz jest nie poprawnie wypełniony');
            return false;
        }

        if($blog->save() == false)
        {
            $this->flashSession->error('Nie udało się zapisać kategorii');
            return false;
        }
        else
        {
            $this->flashSession->success('Kategoria została poprawnie zapisana');
            return $this->response->redirect('blog/editCategory/'.$blog->id);
        }

        $this->view->pick('blog/newCategory');
    }

    public function editCategoryAction($id)
    {
        $blog = BlogCategory::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$blog)
        {
            $this->flashSession->error('Kategoria o podanym id nie istnieje');
            return $this->response->redirect('blog/listCategory');
        }

        $this->view->form = new BlogCategoryForm($blog, array('edit' => true));
    }

    public function updateCategoryAction()
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('blog/listCategory');
        }
        $id = $this->request->getPost('id');

        $blog = BlogCategory::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$blog)
        {
            $this->flashSession->error('Kategoria o podanym id nie istnieje');
            return $this->response->redirect('blog/listCategory');
        }

        $form = new BlogCategoryForm();
        $this->view->form = $form;

        $data = $this->request->getPost();
        $form->bind($data, $blog);

        if(!$form->isValid())
        {
            $this->flashSession->error('Formularz jest nie poprawnie wypełniony');
            return false;
        }

        if($blog->save() == false)
        {
            $this->flashSession->error('Nie udało zaktualizować kategorii');
            return false;
        }
        else
        {
            $this->flashSession->success('Kategoria została pomyślnie zaktualizowana');
            return $this->response->redirect('blog/editCategory/'.$blog->id);
        }

        $this->view->pick('blog/edit');
    }

    public function deleteCategoryAction($id)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('blog/listCategory');
        }

        $category = BlogCategory::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        $child = BlogCategory::find(array('parent = :id:', 'bind' => array('id' => $category->id)));

        if(true) // add param in config
        {
            if(count($child) > 0)
            {
                echo "DANGER||Nie można usnąć kategorii ponieważ posiada inne kategorie w sobie";
                return;
            }
        }
        else
        {
            foreach($child as $item)
            {
                $item->parent = 0;
                $item->save();
            }
        }

        if($category)
        {
            $category->delete();
            echo "SUCCESS||Kategoria została poprawnie usunięta";
        }
        else
            echo "DANGER||Dana kategoria nie istnieje nie można jej usunąć";
    }

    public function enabledCategoryAction($id, $value)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('blog/listCategory');
        }
        
        $category = BlogCategory::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        $category->enabled = $value;

        if($category->save())
        {
            echo "SUCCESS||Zmieniono status kategorii";
        }
        else
            echo "DANGER||Dana kategoria nie istnieje nie można było zastosować zmian";
    }

    public function commentsAction($id)
    {
        if($id > 0) 
        {
            $comments = BlogComment::find(array(
                "blog_id = :id:",
                'bind' => array('id' => $id),
                'order' => 'date_create DESC'
            ));
        }
        else 
        {
            $comments = BlogComment::find(array(
                'order' => 'date_create DESC'
            ));
        }

        $this->view->comments = $comments;
    }

    public function listAction($parentId = 0, $sele = 0)
    {
        $lists = BlogCategory::find(array(
            "parent = 0 AND enabled = 1",
        ));

        $level = 1;

        foreach($lists as $list) {
            if($list->id == $parentId) continue;
            $this->meni[] = array('id' => $list->id, 'level' => $level, 'name' => $list->html_name);
            $this->childListCategory($list->id, $level, $parentId);
        }

        $this->view->meni = $this->meni;
        $this->view->sele = $sele;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    private function childListCategory($parent, $level, $idParent = 0)
    {
        $lists = BlogCategory::find(array(
            'parent = :id: AND enabled = 1',
            'bind' => array('id' => $parent)
        ));

        $level += 1;

        foreach($lists as $list) {
            if($list->id == $idParent) continue;
            $this->meni[] = array('id' => $list->id, 'level' => $level, 'name' => $list->html_name);
            $this->childListCategory($list->id, $level, $idParent);
        }
    }
}