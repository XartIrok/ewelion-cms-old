<?php

namespace CMS\Backend\Controllers;

use CMS\Models\Pages;
use CMS\Backend\Forms\PagesForm;
use Library\myTools;
use Phalcon\Mvc\View;

class PagesController extends ControllerBase
{

    private $meni = array();
    private $lista = array();

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction() {
        $lists = Pages::find(array(
            'parent = :id:',
            'bind' => array('id' => '0'),
            'order' => 'position, lp'
        ));

        $level = 1;

        foreach($lists as $list) {
            $this->lista[] = array('level' => $level, 'page' => $list);
            $this->childList($list->id, $level);
        }

        $this->view->pages = $this->lista;
    }

    private function childList($id, $level) {
        $lists = Pages::find(array(
            'parent = :id:',
            'bind' => array('id' => $id),
            'order' => 'position, lp'
        ));

        $level += 1;

        foreach($lists as $list) {
            $this->lista[] = array('level' => $level, 'page' => $list);
            $this->childList($list->id, $level);
        }
    }

    public function newAction()
    {
        $this->view->form = new PagesForm(null, array('edit' => false));
    }

    public function addAction()
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('pages/index');
        }

        $page = new Pages();
        $form = new PagesForm();

        $data = $this->request->getPost();
        $form->bind($_POST, $page);

        if(!$form->isValid())
        {
            $this->flashSession->error('Formularz jest nie poprawnie wypełniony');
            return false;
        }

        if($page->save() == false)
        {
            $this->flashSession->error('Nie udało się zapisać strony');
            return false;
        }
        else
        {
            $this->flashSession->success('Strona została poprawnie zapisana');
            return $this->response->redirect('pages/edit/'.$page->id);
        }

        $this->view->pick('pages/new');
    }

    public function editAction($id)
    {
        $page = Pages::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$page)
        {
            $this->flashSession->error('Strona o podanym id nie istnieje');
            return $this->response->redirect('pages/index');
        }

        $this->view->linkId = $page->id;
        $this->view->linkTitle = myTools::clearLink($page->html_name);

        $this->view->form = new PagesForm($page, array('edit' => true));
    }

    public function updateAction()
    {
        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('pages/index');
        }
        $id = $this->request->getPost('id');

        $page = Pages::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$page)
        {
            $this->flashSession->error('Strona o podanym id nie istnieje');
            return $this->response->redirect('pages/index');
        }

        $form = new PagesForm();
        $this->view->form = $form;

        $data = $this->request->getPost();
        $form->bind($data, $page);

        if(!$form->isValid())
        {
            $this->flashSession->error('Formularz jest nie poprawnie wypełniony');
            return false;
        }

        if($page->save() == false)
        {
            $this->flashSession->error('Nie udało zaktualizować strony');
            return false;
        }
        else
        {
            $this->flashSession->success('Strona została pomyślnie zaktualizowana');
            return $this->response->redirect('pages/edit/'.$page->id);
        }

        $this->view->pick('pages/edit');
    }

    public function deleteAction($id)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('pages');
        }

        $category = Pages::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        $child = Pages::find(array('parent = :id:', 'bind' => array('id' => $category->id)));

        if(false) // add param in config
        {
            if(count($child) > 0)
            {
                echo "DANGER||Nie można usnąć strony ponieważ posiada inne podstrony w sobie";
                return;
            }
        }
        else
        {
            foreach($child as $item)
            {
                $item->parent = 0;
                $item->save();
            }
        }

        if($category)
        {
            $category->delete();
            echo "SUCCESS||Strona została poprawnie usunięta";
        }
        else
            echo "DANGER||Dana strona nie istnieje nie można jej usunąć";
    }

    public function enabledAction($id, $value)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('pages');
        }
        
        $category = Pages::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        $category->enabled = $value;

        if($category->save())
        {
            echo "SUCCESS||Zmieniono status strony";
        }
        else
            echo "DANGER||Dana strona nie istnieje nie można było zastosować zmian";
    }

    public function listAction($id, $parentId = 0, $sele = 0)
    {
        $lists = Pages::find(array(
            'position = :pos: AND parent = 0',
            'bind' => array('pos' => $id)
        ));

        $level = 1;

        foreach($lists as $list) {
            if($list->id == $parentId) continue;
            $this->meni[] = array('id' => $list->id, 'level' => $level, 'name' => $list->html_name);
            $this->childPages($list->id, $level, $parentId);
        }

        $this->view->meni = $this->meni;
        $this->view->sele = $sele;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    private function childPages($parent, $level, $idParent = 0)
    {
        $lists = Pages::find(array(
            'parent = :id:',
            'bind' => array('id' => $parent)
        ));

        $level += 1;

        foreach($lists as $list) {
            if($list->id == $idParent) continue;
            $this->meni[] = array('id' => $list->id, 'level' => $level, 'name' => $list->html_name);
            $this->childPages($list->id, $level, $idParent);
        }
    }
}
