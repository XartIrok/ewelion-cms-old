<?php

namespace CMS\Backend\Controllers;

use CMS\Models\Upload;
use \Phalcon\Image\Adapter\Gd;

class MainController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {

    }

    public function logoutAction()
    {
        $this->session->remove($this->config->session->name);
        $this->flashSession->success('Dozobaczenia. :)');
        return $this->response->redirect('index');
    }

    public function showPhotoAction($uploadId)
    {
        $this->view->disable();

        $photo = Upload::findFirst(array(
            "id = :id: AND enabled = 1",
            'bind' => array('id' => $uploadId)
        ));
        $date = date_create($photo->date_create);

        $dirFiles   = APP_PATH . 'files'.DIRECTORY_SEPARATOR.$date->format('Y/m/d');
        $dirUpload  = APP_PATH . 'public\uploads\link';
        $dir = ($photo->link) ? $dirUpload : $dirFiles;

        $img = new Gd($dir.DIRECTORY_SEPARATOR.$photo->name_ext);
        $type = $img->getMime();
        $data = $img->render($photo->ext, 90);

        $this->response->setHeader("Content-Type", $type);
        echo $data;
    }

    public function mailAction()
    {
        $message = $this->mailer->createMessage()
            ->to('redbull915@gmail.com', 'Bartek')
            ->subject('test')
            ->content('Lorem');
        if($message->send())
            echo 'send';
        else
            echo 'blad';
        die();
    }
}
