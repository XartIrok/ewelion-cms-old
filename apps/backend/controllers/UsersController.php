<?php

namespace CMS\Backend\Controllers;

use CMS\Backend\Forms\UsersForm;
use CMS\Models\UserAccount;
use CMS\Models\UserInformation;
use CMS\Models\UserProfile;
use Library\myTools;
use Phalcon\Mvc\View;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Tag;

class UsersController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction($page = 1)
    {
        $objects = UserAccount::find(array('order' => 'id'));

        $lists = new Paginator(array(
            'data' => $objects,
            'limit' => 10,
            'page' => $page
        ));

        $this->view->users = $lists->getPaginate();
    }

    public function myAction()
    {
        $sessionUser = $this->session->get($this->config->session->name);
    }

    public function newAction()
    {
        $form = new UsersForm(null, array('edit' => false));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                $userAccount = new UserAccount();
                $userAccount->assign(array(
                    'login' => $this->request->getPost('login', 'striptags'),
                    'pass' => $this->security->hash($this->request->getPost('password'))
                ));
                $userAccount->save();

                $userInformation = new UserInformation();
                $userInformation->assign(array(
                    'user_id' => $userAccount->id
                ));
                $userInformation->save();

                $userProfile = new UserProfile();
                $userProfile->assign(array(
                    'user_id' => $userAccount->id,
                    'email' => $this->request->getPost('mail'),
                    'name' => $this->request->getPost('name'),
                    'surname' => $this->request->getPost('surname')
                ));
                $userProfile->save();

                $this->flashSession->success('Użytkownik został stworzony');
                return $this->response->redirect('users/edit/'.$userAccount->id);
            }
            else {
                $this->flashSession->error('Formularz został nie poprawnie uzupełniony');
            }
        }

        $this->view->form = $form;
    }

    public function editAction($id)
    {
        $id = isset($id) ? $id : $this->request->getPost('id');
        $userAccount = UserAccount::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if (!$userAccount) {
            $this->flashSession->error('Użytkownik nie istnieje');
            return $this->response->redirect('users/index');
        }

        $form = new UsersForm(null, array('edit' => true));

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                $userAccount->assign(array(
                    'login' => $this->request->getPost('login', 'striptags')
                ));
                if ($this->request->getPost('password'))
                    $userAccount->pass = $this->security->hash($this->request->getPost('password'));
                $userAccount->save();

                $userProfile = UserProfile::findFirst(array('user_id = :id:', 'bind' => array('id' => $id)));
                $userProfile->assign(array(
                    'email' => $this->request->getPost('mail'),
                    'name' => $this->request->getPost('name'),
                    'surname' => $this->request->getPost('surname')
                ));
                $userProfile->save();

                $this->flashSession->success('Uzytkownik został edytowany');
                return $this->response->redirect('users/edit/'.$id);
            }
            else {
                $this->flashSession->error('Formularz został nie poprawnie uzupełniony');
            }
        }
        $this->view->user = $userAccount;
        $this->view->form = $form;
    }

    public function enabledAction($id, $value)
    {
        $this->view->disable();
        if ($this->request->isAjax()) {
            $user = UserAccount::findFirst(array('id = :id:', 'bind' => array('id' => $id)));
            $user->enabled = $value;

            if ($user->save())
                echo "SUCCESS||Zmieniono status użytkownika";
            else
                echo "DANGER||Dany użytkownik nie istnieje";
        }
    }

    public function deleteAction($id)
    {
        $this->view->disable();
        if ($this->request->isAjax()) {
            $user = UserAccount::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

            if ($user->delete())
                echo "SUCCESS||Użytkownik została poprawnie usunięta";
            else
                echo "DANGER||Dany użytkownik nie istnieje";
        }
    }
}