<?php 

namespace CMS\Backend\Controllers;

use Library\myTools;
use Phalcon\Tag;
use Phalcon\Mvc\View;

use CMS\Models\GalleryList;
use CMS\Models\GalleryPhoto;
use CMS\Models\UploadAttach;
use CMS\Backend\Forms\GalleryForm;
use CMS\Backend\Forms\GalleryPhotoForm;

class GalleryController extends ControllerBase
{
    protected $countPhoto;

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
        $lists = GalleryList::find(array('order' => 'id'));
        $this->view->lists = $lists;
    }

    public function newAction()
    {
        $form = new GalleryForm(null, array('edit' => false));

        if($this->request->isPost())
        {
            $gallery = new GalleryList();

            $data = $this->request->getPost();
            $form->bind($data, $gallery);

            if(!$form->isValid())
                $this->flashSession->error('Formularz jest nie poprawnie wypełniony');

            if($gallery->save() == false)
                $this->flashSession->error('Nie udało się zapisać artykułu');
            else
            {
                $this->flashSession->success('Artykuł została poprawnie zapisana');
                Tag::resetInput();
            }
        }

        $this->view->form = $form;
    }

    public function editAction($id)
    {
        $gallery = GalleryList::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$gallery)
        {
            $this->flashSession->error('Galeria nie istnieje');
            return $this->response->redirect('gallery/index');
        }

        $photos = GalleryPhoto::find(array('gallery_id = :id:', 'bind' => array('id' => $id)));
        $form = new GalleryForm($gallery, array('edit' => true));

        if($this->request->isPost())
        {
            $data = $this->request->getPost();
            $form->bind($data, $gallery);

            if(!$form->isValid())
                $this->flashSession->error('Formularz jest nie poprawnie wypełniony');

            if(!$gallery->save())
                $this->flashSession->error('Nie udało się zapisać galerii');
            else
            {
                $this->flashSession->success('Galeria została poprawnie zapisana');
                Tag::resetInput();
            }
        }

        $this->view->form = $form;
        $this->view->gallery = $id;
        $this->view->photos = $photos;
    }

    public function enabledAction($id, $value)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('gallery');
        }
        
        $gallery = GalleryList::findFirst(array('id = :id:', 'bind' => array('id' => $id)));
        $gallery->enabled = $value;

        if($gallery->save())
            echo "SUCCESS||Zmieniono status galerii";
        else
            echo "DANGER||Dana galeria nie istnieje nie można było zastosować zmian";
    }

    public function deleteAction($id)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('gallery');
        }

        $photo = GalleryList::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if($photo->delete()) {
            $photos = GalleryPhoto::find(array('gallery_id = :id:', 'bind' => array('id' => $id)));
            $photos->delete();
            echo "SUCCESS||Zdjęcie została poprawnie usunięta";
        }
        else
            echo "DANGER||Dana zdjęcie nie istnieje nie można jej usunąć";
    }

    public function clearAction($id)
    {
        $photos = GalleryPhoto::find(array('gallery_id = :id:', 'bind' => array('id' => $id)));

        if($photos)
        {
            $photos->delete();
            $this->flashSession->success('Galeria została wyczyszczona');
            return $this->response->redirect('gallery/edit/'.$id);
        }
        else
        {
            $this->flashSession->success('W galeri brak zdjęci do usunięcia');
            return $this->response->redirect('gallery/edit/'.$id);
        }
    }

    public function addPhotoAction()
    {
        $gallery = $this->request->getPost('gallery');
        $images = $this->request->getPost('images');
        // TODO: pomysleć nad wyświetlaniem nazw dodanych zdjęć
        $this->countPhoto = 0;

        if(is_array($images))
        {
            foreach ($images as $image) {
                $this->addPhoto($gallery, $image);
            }
        }
        else
            $this->addPhoto($gallery, $images);

        $this->session->remove('imageCheck');
        
        $this->flashSession->success('Do galerii dodano '.$this->countPhoto.' zdjęć');
        return $this->response->redirect('gallery/edit/'.$gallery);
    }

    private function addPhoto($gallery, $image)
    {
        $photo = new GalleryPhoto();
        $photo->gallery_id = $gallery;
        $photo->upload_id = $image;

        $atta = new UploadAttach();
        $atta->upload_id = $image;
        $atta->where_is = 3;
        $atta->photo = 1;

        if($photo->save())
        {
            $atta->field_id = $photo->id;
            $atta->save();
            $this->countPhoto++;
        }
    }

    public function editPhotoAction($id)
    {
        $photo = GalleryPhoto::findFirst(array('id = :id:', 'bind' => array('id' => $id)));

        if(!$photo)
        {
            $this->flashSession->error('Zdjęcie o podanym id nie istnieje');
            return $this->response->redirect('blog/index');
        }

        $form = new GalleryPhotoForm($photo, array('edit' => true));

        if($this->request->isPost())
        {
            $data = $this->request->getPost();
            $form->bind($data, $photo);

            if(!$form->isValid())
                $this->flashSession->error('Formularz jest nie poprawnie wypełniony');

            if($photo->save() == false)
                $this->flashSession->error('Nie udało się edytować zdjęcia');
            else
            {
                $this->flashSession->success('Zdjęcie została edytowane');
                Tag::resetInput();
            }
        }

        $this->view->form = $form;
    }

    public function enabledPhotoAction($gallery, $id, $value)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('gallery/edit/'.$gallery);
        }
        
        $photo = GalleryPhoto::findFirst(array('id = :id:', 'bind' => array('id' => $id)));
        $photo->enabled = $value;

        if($photo->save())
            echo "SUCCESS||Zmieniono status zdjęcie";
        else
            echo "DANGER||Dana zdjęcie nie istnieje nie można było zastosować zmian";
    }

    public function deletePhotoAction($gallery, $id)
    {
        $this->view->disable();

        if(!$this->request->isPost())
        {
            $this->flashSession->error('[Request method] POST');
            return $this->response->redirect('gallery/edit/'.$gallery);
        }

        $photo = GalleryPhoto::findFirst(array('id = :id:', 'bind' => array('id' => $id)));
        $atta  = UploadAttach::findFirst(array('field_id = :id: AND where_is = 3 AND photo = 1', 'bind' => array('id' => $id)));
        
        if($photo->delete() && $atta->delete())
            echo "SUCCESS||Zdjęcie została poprawnie usunięta";
        else
            echo "DANGER||Dana zdjęcie nie istnieje nie można jej usunąć";

    }
}