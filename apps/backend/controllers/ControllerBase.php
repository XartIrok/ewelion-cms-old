<?php

namespace CMS\Backend\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    public function initialize() 
    {
        $this->url->setBaseUri('/admin/');
        $this->tag->setTitle('Panel administracyjny');
        
        if($this->session->get($this->config->session->name))
        {
            $this->assets->get('style')->addCss('css/admin/style.css', true);
            $this->assets->get('style')->addCss('css/admin/forms.css', true);

            $this->assets->get('jsHead')->addJs('components/bootstrap/dist/js/bootstrap.min.js', true);
            $this->assets->get('jsHead')->addJs('components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js', true);
            
            $this->assets->get('jsTools')->addJs('js/tools.js', true);

            $this->view->menu = $this->config->menu;
        }
        else
        {
            if($this->router->getControllerName() != 'index' && $this->router->getActionName() != 'index')
            {
                $this->flashSession->error('Nie masz uprawnień do przebywania na tej stronie lub twoja sesja już wygasła');
                $this->response->redirect('index');
            }
        }
    }
    
}
