<?php
return new \Phalcon\Config(array(
    'application' => array(
        'controllersDir' => __DIR__ . '/../controllers/',
        'modelsDir' => __DIR__ . '/../models/',
        'viewsDir' => __DIR__ . '/../views/',
        'baseUri' => '/'
    ),
    'session' => array(
        'name' => 'admin_cms'
    ),
    'menu' => array(
        'main' => array(
            'active' => true,
            'link' => 'main',
            'name' => 'Panel',
            'icon' => 'home'
        ),
        'pages' => array(
            'active' => true,
            'link' => 'pages',
            'name' => 'Strony',
            'icon' => 'file-text-o',
            'actions' => array(
                array('link' => 'pages/new', 'name' => 'Dodaj stronę', 'icon' => 'plus'),
                array('link' => 'pages', 'name' => 'Lista stron', 'icon' => 'bars')
            )
        ),
        'blog' => array(
            'active' => true,
            'link' => 'blog',
            'name' => 'Blog',
            'icon' => 'newspaper-o',
            'actions' => array(
                array('link' => 'blog/new', 'name' => 'Dodaj wpis', 'icon' => 'plus'),
                array('link' => 'blog', 'name' => 'Lista wpisów', 'icon' => 'bars'),
                array('separate' => 'separate'),
                array('link' => 'blog/newCategory', 'name' => 'Dodaj kategorie', 'icon' => 'plus'),
                array('link' => 'blog/listCategory', 'name' => 'Lista kategorii', 'icon' => 'bars'),
                array('separate' => 'separate'),
                array('link' => 'blog/comments', 'name' => 'Komentarze', 'icon' => 'comments'),
            )
        ),
        'files' => array(
            'active' => true,
            'link' => 'files',
            'name' => 'Pliki',
            'icon' => 'file'
        ),
        'gallery' => array(
            'active' => true,
            'link' => 'gallery',
            'name' => 'Galerie',
            'icon' => 'camera',
            'actions' => array(
                array('link' => 'gallery/new', 'name' => 'Dodaj galerie', 'icon' => 'plus'),
                array('link' => 'gallery', 'name' => 'Lista galerii', 'icon' => 'bars')
            )
        ),
        'users' => array(
            'active' => true,
            'link' => 'users',
            'name' => 'Użytkownicy',
            'icon' => 'users'
        ),
    )
));
