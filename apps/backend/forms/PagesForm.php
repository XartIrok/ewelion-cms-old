<?php

namespace CMS\Backend\Forms;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;
use CMS\Models\Position;

class PagesForm extends FormBase
{

    public function initialize($entity = null, $options = null)
    {
        if($options['edit'] == true) {
            $this->add(new Hidden('id'));
        }
        
        $lang = new Hidden('lang_code');
        $this->add($lang);
        
        $name = new Text('html_name');
        $this->add($name);
        
        $desc = new TextArea('html_desc');
        $this->add($desc);
        
        $posi = new Select('position', Position::find(), array(
                'using' => array('id', 'name')
        ));
        $this->add($posi);
        
        $pare = new Select('parent', array('0' => ''));
        $this->add($pare);
        
        $enab = new Radio('enabled');
        $this->add($enab);
        
        $styp = new Select('show_typ', array('0' => 'Tekst', '1' => 'Tekst + Formularz'));
        $this->add($styp);
        
        $snam = new Radio('show_name');
        $this->add($snam);
        
        $poup = new Radio('show_popup');
        $this->add($poup);
        
        $link = new Text('link_referrer');
        $this->add($link);
        
        $nwin = new Radio('new_window');
        $this->add($nwin);
        
        $dcli = new Radio('dont_click');
        $this->add($dcli);
        
        $seot = new Text('seo_title');
        $this->add($seot);
        
        $seok = new Text('seo_key');
        $this->add($seok);
        
        $seod = new TextArea('seo_desc');
        $this->add($seod);
    }

}
