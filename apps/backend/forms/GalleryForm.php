<?php

namespace CMS\Backend\Forms;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;

class GalleryForm extends FormBase
{

    public function initialize($entity = null, $options = null)
    {
        if($options['edit'] == true) {
            $this->add(new Hidden('id'));
        }
        
        $session = $this->session->get($this->config->session->name);
        $user = new Hidden('user_id', array('value' => $session['id']));
        $this->add($user);

        $name = new Text('html_name');
        $this->add($name);

        $enab = new Radio('enabled');
        $this->add($enab);

        $enab = new Radio('show_name');
        $this->add($enab);
        
    }

}
