<?php

namespace CMS\Backend\Forms;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Radio;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Uniqueness;

class UsersForm extends FormBase
{

    public function initialize($entity = null, $options = null)
    {
        if ($options['edit'] == true) {
            $this->add(new Hidden('id'));
        }

        $name = new Text('login');
        $name->setLabel('Login');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The name is required'
            ))
        ));
        $this->add($name);

        $imie = new Text('name');
        $imie->setLabel('Imię');
        $this->add($imie);

        $nazwisko = new Text('surname');
        $nazwisko->setLabel('Nazwisko');
        $this->add($nazwisko);

        $email = new Text('mail');
        $email->setLabel('Email');
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));
        $this->add($email);

        $password = new Password('password');
        $password->setLabel('Hasło');
        if ($options['edit'] == false)
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'The password is required'
            )),
            new StringLength(array(
                'min' => 8,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            )),
            new Confirmation(array(
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'repeat'
            ))
        ));
        $this->add($password);

        $confirmPassword = new Password('repeat');
        $confirmPassword->setLabel('Powtórz hasło');
        if ($options['edit'] == false)
        $confirmPassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'The confirmation password is required'
            ))
        ));
        $this->add($confirmPassword);
    }

}
