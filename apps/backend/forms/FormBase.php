<?php

namespace CMS\Backend\Forms;

use Phalcon\Forms\Form;

class FormBase extends Form
{
    public function newRender($name, $attr = array())
    {
        $element = $this->get($name);
        $element->setAttributes($attr);
        $messages = $this->getMessagesFor($element->getName());

        $label = (isset($attr['label'])) ? $attr['label'] : $element->getLabel();

        echo '<div class="form-group">';
        echo '<label class="col-xs-5 control-label">'.$element->getLabel().':</label>';
        echo '<div class="col-xs-7">';
        echo $element;

        if (count($messages)) {
            foreach ($messages as $message) {
                echo '<span class="help-block label label-danger">';
                echo $message;
                echo '</span> ';
            }
        }

        echo '</div>';
        echo '</div>';
    }
}