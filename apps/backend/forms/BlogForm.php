<?php

namespace CMS\Backend\Forms;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Radio;

class BlogForm extends FormBase
{

    public function initialize($entity = null, $options = null)
    {
        if($options['edit'] == true) {
            $this->add(new Hidden('id'));
        }
        
        $lang = new Hidden('lang_code');
        $this->add($lang);
        
        $name = new Text('html_name');
        $this->add($name);
        
        $desc = new TextArea('html_desc');
        $this->add($desc);
        
        $dess = new TextArea('html_desc_short');
        $this->add($dess);

        $enab = new Radio('enabled');
        $this->add($enab);

        $enab = new Radio('highlights');
        $this->add($enab);

        $enab = new Radio('accept');
        $this->add($enab);

        $enab = new Radio('block_comment');
        $this->add($enab);
        
        $seot = new Text('seo_title');
        $this->add($seot);
        
        $seok = new Text('seo_key');
        $this->add($seok);
        
        $seod = new TextArea('seo_desc');
        $this->add($seod);
    }

}
