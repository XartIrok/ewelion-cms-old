<?php

namespace CMS\Frontend;

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Assets\Manager as AssetsManager;

class Module
{

	public function registerAutoloaders()
	{

		$loader = new Loader();

		$loader->registerNamespaces(array(
			'CMS\Frontend\Controllers' 	=> '../apps/frontend/controllers/',
			'CMS\Models' 				=> '../apps/models/',
            'Library'                   => '../library/',
		));

		$loader->register();
	}

	/**
	 * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
	 */
	public function registerServices($di)
	{

		//Registering a dispatcher
		$di->set('dispatcher', function () {
			$dispatcher = new Dispatcher();

			//Attach a event listener to the dispatcher
			$eventManager = new \Phalcon\Events\Manager();
			//$eventManager->attach('dispatch', new \Acl('frontend'));

			$dispatcher->setEventsManager($eventManager);
			$dispatcher->setDefaultNamespace("CMS\Frontend\Controllers\\");
			return $dispatcher;
		});

		//Registering the view component
		$di->set('view', function () {
			$view = new View();
			$view->setViewsDir('../apps/frontend/views/');
			return $view;
		});

        $di->set('assets', function () {
            $assets = new AssetsManager();

            $assets->collection('style')
                ->addCss('css/main/bootstrap.min.css', true)
                ->addCss('css/main/jasny-bootstrap.min.css', true)
                ->addCss('css/main/font-awesome.min.css', true)
                ->addCss('css/frontend/materialize.min.css', true)
                ->addCss('css/frontend/style.css', true);
             // ->setSourcePath('css/frontend/main.css')
             // ->setTargetUri('css/frontend/main.css')
             // ->join(true)
             // ->addFilter(new \Phalcon\Assets\Filters\Cssmin());
            
            $assets->collection('jsHead')
                ->addJs('js/jquery-2.1.4.min.js', true)
                ->addJs('js/materialize.min.js', true);
            
            $assets->collection('jsTools');
            
            return $assets;
        });

	}
}
