<?php

namespace CMS\Frontend\Controllers;

use CMS\Models\UserAccount as UserAccount;
use CMS\Models\UserInformation as UserInformation;

class UsersController extends ControllerBase
{

    public function indexAction()
    {
		if($this->session->get('auth'))
			$this->response->redirect('users/manager');
		else
			$this->response->redirect('users/login');
    }
    
    private function auth(UserAccount $user)
    {
        $this->session->set('auth', array(
            'id' => $user->id,
            'name' => $user->login
        ));
    }
    
    public function loginAction()
    {
    	if($this->request->isPost())
    	{
            $logi = $this->request->getPost('logi');
            $pass = $this->request->getPost('pass');
            
    		$firstSearch = UserAccount::findFirst(array(
    				"login = :login: AND is_active = 1",
    				'bind' => array('login' => $logi)
    		));
    		
    		if($firstSearch != false)
    		{
                $pass = hash($firstSearch->algoi, $pass.$firstSearch->salt);
                
                $secondSearch = UserAccount::findFirst(array(
                    "pass = :pass: AND is_active = 1",
                    'bind' => array('pass' => $pass)    
                ));
                
                if($secondSearch != false)
                {   
                    $this->auth($secondSearch);
                    
                    $userInfo = $secondSearch->userInformation;
                    $userInfo->date_lastactive = time();
                    $userInfo->save();
                    
                    $this->flashSession->success('Witaj: '.$logi);
                    return $this->response->redirect('users/manager');
                }
    		}
    		
    		$this->flashSession->error('Błędny login lub hasło');
    	}
    }
    
    public function registerAction()
    {
        if($this->request->isPost())
        {
            $algo = 'sha1';
            $pref = 'cms_'.time().'_';
            
            $logi = $this->request->getPost('logi');
            $pass = $this->request->getPost('pass');
            $pasr = $this->request->getPost('pasr');

            if($pass != $pasr) {
                $this->flashSession->error('Hasła nie są identycznę');
                return false;
            }
            
            $salt = hash($algo, uniqid($pref, true));
            $pass = hash($algo, $pass.$salt);
            
            if($logi && $pass)
            {
                $user = new UserAccount();
                $user->login = $logi;
                $user->algoi = $algo;
                $user->pass = $pass;
                $user->salt = $salt;
                $user->is_active = 1;
                
                if($user->save() == false)
                {
                    
                }
                else
                {
                    $userInfo = new UserInformation();
                    $userInfo->user_id = $user->id;
                    $userInfo->group_id = 1;
                    $userInfo->date_create = time();
                    $userInfo->save();
                    
                    $this->flashSession->success('Udało się założyć konto');
                    return $this->response->redirect('users/index');
                }
            }
            $this->flashSession->error('Formularz musi być uzupełniony');
        }
    }
    
    public function logoutAction()
    {
        $this->session->remove('auth');
        $this->flashSession->success('Dozobaczenia. :)');
        return $this->response->redirect('users/login');
    }

}

