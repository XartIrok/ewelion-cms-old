<?php

namespace CMS\Models;
use Phalcon\Mvc\Model;

class UserInformation extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $group_id;

    /**
     *
     * @var integer
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $date_lastactive;

    /**
     *
     * @var integer
     */
    public $ip_lastactive;

    /**
     *
     * @var string
     */
    public $email_code;

    /**
     *
     * @var integer
     */
    public $emial_time;

    /**
     *
     * @var string
     */
    public $country_code;
    
    public function initialize()
    {
        $this->hasOne('user_id', 'CMS\Models\UserAccount', 'id', array(
            'alias' => 'userAccount'
        ));
    }

}
