<?php
namespace CMS\Models;

class GalleryPhoto extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $gallery_id;

    /**
     *
     * @var integer
     */
    public $upload_id;

    /**
     *
     * @var string
     */
    public $html_name;

    /**
     *
     * @var string
     */
    public $html_desc;

    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var string
     */
    public $link_referrer;

    public function initialize() {
        $this->hasOne('upload_id', 'CMS\Models\Upload', 'id', array(
            'alias' => 'upload'
        ));
    }
    
}
