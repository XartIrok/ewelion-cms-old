<?php

namespace CMS\Models;
use Phalcon\Mvc\Model\Validator\Email as Email;

class UserProfile extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $email_new;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $surname;

    /**
     *
     * @var string
     */
    public $about;

    /**
     *
     * @var string
     */
    public $link_referrer;

    /**
     *
     * @var integer
     */
    public $gender;

    /**
     *
     * @var integer
     */
    public $date_birth;

    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

}
