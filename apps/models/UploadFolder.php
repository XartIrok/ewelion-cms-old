<?php
namespace CMS\Models;

class UploadFolder extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $html_name;

    /**
     *
     * @var integer
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $parent;

    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var integer
     */
    public $undelete;

}
