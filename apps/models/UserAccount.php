<?php

namespace CMS\Models;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;
use CMS\Models\UserProfile;
use CMS\Models\UserInformation;

class UserAccount extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $login;

    /**
     *
     * @var string
     */
    public $pass;

    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var integer
     */
    public $is_admin;
    
    public function initialize()
    {
        $this->hasOne('id', 'CMS\Models\UserInformation', 'user_id', array(
            'alias' => 'userInformation',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE
            )
        ));
        $this->hasOne('id', 'CMS\Models\UserProfile', 'user_id', array(
            'alias' => 'userProfile',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE
            )
        ));
    }

    public function getProfile()
    {
        return UserProfile::findFirst(array(
            "user_id = :id:",
            'bind' => array('id' => $this->id)
        ));
    }

    public function getInformation()
    {
        return UserInformation::findFirst(array(
            "user_id = :id:",
            'bind' => array('id' => $this->id)
        ));
    }
}
