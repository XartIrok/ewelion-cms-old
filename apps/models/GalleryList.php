<?php
namespace CMS\Models;

class GalleryList extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $lang_code;

    /**
     *
     * @var string
     */
    public $html_name;

    /**
     *
     * @var string
     */
    public $html_desc;

    /**
     *
     * @var integer
     */
    public $page_id;

    /**
     *
     * @var integer
     */
    public $blog_id;

    /**
     *
     * @var integer
     */
    public $other_id;

    /**
     *
     * @var integer
     */
    public $position_id;

    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var integer
     */
    public $show_typ;

    /**
     *
     * @var integer
     */
    public $show_name;

    /**
     *
     * @var integer
     */
    public $show_desc;

    /**
     *
     * @var integer
     */
    public $date_create;

}
