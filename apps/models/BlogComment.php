<?php
namespace CMS\Models;

class BlogComment extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $blog_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $html_nick;

    /**
     *
     * @var string
     */
    public $html_desc;

    /**
     *
     * @var integer
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $parent;

    /**
     *
     * @var integer
     */
    public $accept;

    /**
     *
     * @var integer
     */
    public $enabled;

}
