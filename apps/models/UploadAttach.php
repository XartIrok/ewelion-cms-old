<?php
namespace CMS\Models;

class UploadAttach extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $upload_id;

    /**
     *
     * @var integer
     */
    public $field_id;

    /**
     *
     * @var integer
     */
    public $where_is;

    /**
     *
     * @var integer
     */
    public $order;

    /**
     *
     * @var integer
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $photo;

    /**
     *
     * @var integer
     */
    public $sound;

    /**
     *
     * @var integer
     */
    public $attach;

}
