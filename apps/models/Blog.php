<?php
namespace CMS\Models;

class Blog extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $lang_code;

    /**
     *
     * @var string
     */
    public $html_name;

    /**
     *
     * @var string
     */
    public $html_desc;

    /**
     *
     * @var string
     */
    public $html_desc_short;

    /**
     *
     * @var integer
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $category_id;

    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var integer
     */
    public $highlights;

    /**
     *
     * @var integer
     */
    public $accept;

    /**
     *
     * @var integer
     */
    public $block_comment;

    /**
     *
     * @var string
     */
    public $seo_title;

    /**
     *
     * @var string
     */
    public $seo_desc;

    /**
     *
     * @var string
     */
    public $seo_key;

}
