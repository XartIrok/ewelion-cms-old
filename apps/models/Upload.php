<?php
namespace CMS\Models;

class Upload extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $folder_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $hash;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var integer
     */
    public $link;

}
