<?php
namespace CMS\Models;

use CMS\Models\Position;

class Pages extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $lang_code;

    /**
     *
     * @var string
     */
    public $html_name;

    /**
     *
     * @var string
     */
    public $html_desc;

    /**
     *
     * @var integer
     */
    public $position;

    /**
     *
     * @var integer
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $parent;

    /**
     *
     * @var integer
     */
    public $lp;

    /**
     *
     * @var integer
     */
    public $styl;

    /**
     *
     * @var integer
     */
    public $enabled;

    /**
     *
     * @var integer
     */
    public $show_typ;

    /**
     *
     * @var integer
     */
    public $show_name;

    /**
     *
     * @var integer
     */
    public $show_popup;

    /**
     *
     * @var string
     */
    public $link_referrer;

    /**
     *
     * @var integer
     */
    public $new_window;

    /**
     *
     * @var integer
     */
    public $dont_click;

    /**
     *
     * @var string
     */
    public $seo_title;

    /**
     *
     * @var string
     */
    public $seo_desc;

    /**
     *
     * @var string
     */
    public $seo_key;

    
    public function getPositionName() {
        $pos = Position::findFirst(array("id = :id:", 'bind' => array('id' => $this->position)));
        return $pos->name;
    }

    public function beforeSave() {
        $max = Pages::maximum(array("column" => "lp"));
        if(empty($this->lp))
            $this->lp = $max + 1;
    }
}
