<?php
error_reporting(E_ALL);

(new Phalcon\Debug())->listen();

require __DIR__ . '/../config/loader.php';

$application = new Application();
$application->main();
