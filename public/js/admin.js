var times = 500

function cycleImages() {
	var $active = $('.login-form .img .active');
	var $next = ($('.login-form .img .active').next().length > 0) ? $('.login-form .img .active').next() : $('.login-form .img img:first');
	$next.css('z-index', 2);
	$active.fadeOut(times, function() {
		$active.css('z-index', 1).show().removeClass('active');
		$next.css('z-index', 3).addClass('active');
	});
}

$(window).load(function() {
	$('.login-form .img').fadeIn(times);
	setInterval('cycleImages()', 7000);
})