var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight || e.clientHeight || g.clientHeight;
var h = y - parseFloat($('header').css('height'))

var flagMenuDesktop = $('.show-600 .a-menu').hasClass('active')
var flagMenuMobiles = $('.hidden-600 .a-menu').hasClass('active')
    
var flagMenu = $('.a-menu').hasClass('active')
var mobileScript = null
    
if(!flagMenu) {
    if(x <= 360) {
        $('.mainpanel').css({'margin-left': 0})
        $('.leftpanel').css({'left': 0})
        $('.leftpanel').hide()
        $('.a-menu').removeClass('active')
    } else {
        $('.show-600 .a-menu').addClass('active')
        $('.hidden-600 .a-menu').removeClass('active')
        $('.leftpanel').show()
        $('.leftpanel').css({'left': 0})
        $('.mainpanel').css({'margin-left': parseFloat($('.leftpanel').css('width'))})
    }
    flagMenu = $('.a-menu').hasClass('active')
    flagMenuDesktop = $('.show-600 .a-menu').hasClass('active')
    flagMenuMobiles = $('.hidden-600 .a-menu').hasClass('active')
}

$(window).resize(function() {
    w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight || e.clientHeight || g.clientHeight;
    h = y - parseFloat($('header').css('height'))

    // $('.mainpanel').css('min-height', h)
    
    if(flagMenu) 
    {
        if(x <= 360) {
            if(!mobileScript) {
                $('.mainpanel').css({'margin-left': 0})
                $('.leftpanel').css({'left': 0})
                $('.leftpanel').hide()
            }
        } else {
            $('.leftpanel').show()
            $('.leftpanel').animate({'left': 0})
            $('.mainpanel').css({'margin-left': '240px'})
        }
    }
    else {
        if(x <= 360) {
            $('.mainpanel').css({'margin-left': 0})
            $('.leftpanel').css({'left': 0})
            $('.leftpanel').hide()
        } else {
            $('.leftpanel').hide()
            $('.leftpanel').css({'left': -(parseFloat($('.leftpanel').css('width')))})
            $('.mainpanel').css({'margin-left': '0'})
        }
    }

    if(x <= 360) {
        mobileScript = true
    }
    else {
        mobileScript = false
    }
    
    flagMenu = $('.a-menu').hasClass('active')
});

if(x <= 360) {
    mobileScript = true
}
else {
    mobileScript = false
}

//$('.mainpanel').css('min-height', h)

$('.leftpanel nav .parent').on('click', function() {
    var flag = $(this).parent().hasClass('active')

    $('nav .nav-parent').removeClass('active')
    $('nav .nav-parent span.parent').removeClass('fa-chevron-down')
    $('nav .nav-parent span.parent').addClass('fa-chevron-left')
    $('nav .nav-parent .dropdown-menu').slideUp()

    if(!flag) {
        $(this).parent().addClass('active')
        $(this).removeClass('fa-chevron-left')
        $(this).addClass('fa-chevron-down')
        $(this).parent().find('.dropdown-menu').slideDown()
    }
})

$('.leftpanel nav .a-parent').on('click', function(e) {
    var flag = $(this).parent().hasClass('active')

    $('nav .nav-parent').removeClass('active')
    $('nav .nav-parent span.parent').removeClass('fa-chevron-down')
    $('nav .nav-parent span.parent').addClass('fa-chevron-left')
    $('nav .nav-parent .dropdown-menu').slideUp()

    if(!flag) {
        $(this).parent().addClass('active')
        $(this).parent().find('span.parent').removeClass('fa-chevron-left')
        $(this).parent().find('span.parent').addClass('fa-chevron-down')
        $(this).parent().find('.dropdown-menu').slideDown()
    }

    e.preventDefault();
})

if($('.leftpanel nav .nav-parent').hasClass('active')) {
    $('.leftpanel nav .nav-parent.active').find('span.parent').removeClass('fa-chevron-left')
    $('.leftpanel nav .nav-parent.active').find('span.parent').addClass('fa-chevron-down')
    $('.leftpanel nav .nav-parent.active').find('.dropdown-menu').show()
}

$('.a-menu').on('click', function(e) {
    if(x > 360) 
    {
        if(flagMenu) {
            $(this).removeClass('active')
            $('.leftpanel').animate({'left': -(parseFloat($('.leftpanel').css('width')))})
            $('.mainpanel').animate({'margin-left': 0})
        } else {
            $(this).addClass('active')
            $('.leftpanel').show()
            $('.leftpanel').animate({'left': '0'})
            $('.mainpanel').animate({'margin-left': parseFloat($('.leftpanel').css('width'))})
        }
    } else {
        if(flagMenu) {
            $(this).removeClass('active')
            $('.leftpanel').slideUp()
        } else {
            $('.leftpanel').css({'left': '0'})
            $('.mainpanel').animate({'margin-left': 0})
            $(this).addClass('active')
            $('.leftpanel').slideDown()
        }
    }

    flagMenu = $(this).hasClass('active')
    
    e.preventDefault();
})

$('.mainpanel').on('click', function() {
    if(x > 360 && x < 1200) {
        $('.a-menu').removeClass('active')
        $('.leftpanel').animate({'left': '-240px'})
        $('.mainpanel').animate({'margin-left': 0})
    }
    flagMenu = $('.a-menu').hasClass('active')
})

$('.box .box-title .fa').on('click', function() {
    var parent = $(this).parent().parent();
    var flag = parent.hasClass('active')

    if(flag) {
        parent.find('.box-content').slideUp()
        parent.removeClass('active')
    }
    else {
        parent.find('.box-content').slideDown()
        parent.addClass('active')
    }
})

if($("#main-text").is(':hidden')) {
    $(".tab-pane textarea").removeAttr("disabled");
    $("#main-text textarea").attr("disabled", "disabled");
}
else {
    $(".tab-pane textarea").attr("disabled", "disabled");
    $("#main-text textarea").removeAttr("disabled");
}

$( window ).resize(function() {
    if($("#main-text").is(':hidden')) {
        $(".tab-pane textarea").removeAttr("disabled");
        $("#main-text textarea").attr("disabled", "disabled");
    }
    else {
        $(".tab-pane textarea").attr("disabled", "disabled");
        $("#main-text textarea").removeAttr("disabled");
    }
});

