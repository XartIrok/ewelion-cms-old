<?php

// change name file to config.local.php or config.prod.php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => '',
        'password' => '',
        'dbname' => ''
    ),
    'mailer' => array(
        'driver' => 'smtp',
        'host' => 'smtp.example.com',
        'port' => 465,
        'encryption' => 'ssl',
        'username' => 'example@example.com',
        'password' => 'exmaple',
        'from' => array(
            'email' => 'example@example.com',
            'name' => 'example'
        )
    )
));
