<?php

$router->add('/:controller/:action', array(
    'module'     => 'frontend',
    'controller' => 1,
    'action'     => 2,
));

$router->add('/:controller/:action/:params', array(
    'module'     => 'frontend',
    'controller' => 1,
    'action'     => 2,
    'params'     => 3,
));

$router->add('/admin', array(
    'module'     => 'backend',
));

$router->add('/admin/:controller', array(
    'module'     => 'backend',
    'controller' => 1,
    'action'     => 'index',
));

$router->add('/admin/:controller/:action/:params', array(
    'module'     => 'backend',
    'controller' => 1,
    'action'     => 2,
    'params'     => 3,
));

$router->add('/{title}/p/{id}', array(
    'module'     => 'frontend',
    'controller' => 'main',
    'action'     => 'index',
    'id'         => 3,
))->setName('page-link');

$router->add('/{title}/b/{id}', array(
    'module'     => 'frontend',
    'controller' => 'blog',
    'action'     => 'article',
    'id'         => 3,
))->setName('article-link');