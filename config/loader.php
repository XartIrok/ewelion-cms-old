<?php 

use Phalcon\Loader;
use Phalcon\Db\Adapter\Pdo\Mysql as Database;
use Phalcon\DI\FactoryDefault;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Application as BaseApplication;
use Phalcon\Mailer\Manager as MailerSend;
use Phalcon\Security;
use Phalcon\Session\Adapter\Files as SessionAdapter;

class Application extends BaseApplication
{

    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    protected function registerServices()
    {
        define('APP_PATH', realpath('..') . DIRECTORY_SEPARATOR);

        $di = new FactoryDefault();
        $loader = new Loader();

        if(getenv('SERVER_NAME') == 'localhost')
            $config = require __DIR__ . DIRECTORY_SEPARATOR ."config.local.php";
        else
            $config = require __DIR__ . DIRECTORY_SEPARATOR ."config.prod.php";

        include_once APP_PATH.'plugins/autoload.php';
        /**
         * We're a registering a set of directories taken from the configuration file
         */
        $loader->registerNamespaces(
            array(

            )
        );

        $eventsManager = new EventsManager();
        $eventsManager->attach('loader', function ($event, $loader) {
           if ($event->getType() == 'beforeCheckPath') {
               echo $loader->getCheckedPath();
           }
        });

        $loader->setEventsManager($eventsManager);
        $loader->register();

        //Registering a router
        $di->set('router', function() {
            $router = new Router();
            $router->removeExtraSlashes(true);
            $router->setDefaultModule("frontend");
            require __DIR__ . '/routes.php';

            return $router;
        });

        $di->set('flash', function () {
            $flash = new \Phalcon\Flash\Direct(array(
                'error' => 'alert alert-error',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-info'
            ));
            return $flash;
        });

        $di->set('flashSession', function(){
            $flash = new \Phalcon\Flash\Session(array(
                    'success' => 'alert alert-success alert-fixed-top',
                    'notice' => 'alert alert-info alert-fixed-top',
                    'warning' => 'alert alert-warning alert-fixed-top',
                    'error' => 'alert alert-danger alert-fixed-top',
            ));
            return $flash;
        });

        $di->set('db', function () use ($config) {
            $db = new Database(array(
                "host" => $config->database->host,
                "username" => $config->database->username,
                "password" => $config->database->password,
                "dbname" => $config->database->dbname
            ));
            $db->query('SET NAMES utf8');

            return $db;
        });

        $di->set('session', function () {
            $session = new SessionAdapter();
            $session->start();

            return $session;
        });

        $di->set('security', function() {
            $security = new Security();
            $security->setWorkFactor(8);

            return $security;
        });

        $di->set('url', function () {
            $url = new UrlResolver();
            $url->setBaseUri('/');

            return $url;
        });

        $di->set('mailer', function () use ($config) {
            return new MailerSend($config->mailer->toArray());
        });

        $this->setDI($di);
    }

    public function main()
    {

        $this->registerServices();

        //Register the installed modules
        $this->registerModules(array(
            'frontend' => array(
                'className' => 'CMS\Frontend\Module',
                'path' => '../apps/frontend/Module.php'
            ),
            'backend' => array(
                'className' => 'CMS\Backend\Module',
                'path' => '../apps/backend/Module.php'
            )
        ));

        echo $this->handle()->getContent();
    }

}